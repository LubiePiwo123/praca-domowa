<?php
class Bazowa
{
public function napis()
{
echo 'Tekst pochodzi z klasy bazowej';
}
}
class Potomna extends Bazowa
{
public function napis()
{
echo '<br>';
parent::napis();
echo ' - wywołany z klasy potomnej...';
echo '<br>Tekst pochodzi z klasy potomnej';
}
}
$objA=new Bazowa();
$objB=new Potomna();
echo $objA->napis();
echo '<br>';
echo $objB->napis();
echo '<br>';
?>